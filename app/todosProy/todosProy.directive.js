/**
 * Created by maxi on 4/3/16.
 */

function printList(){
    return {
        restrict: 'EA',
        templateUrl: 'app/todosProy/taskList.template.html',
        scope: true
    };
}

todosProy.directive('printList', printList);