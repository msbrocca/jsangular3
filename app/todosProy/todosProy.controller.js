/**
 * Created by maxi on 4/2/16.
 */


// Todo List Controller
var todoListController = function (theScope, Service, http, serviceUrl) {

    http.get(serviceUrl + 'tasks/', {}
    ).then(function (response) {
        console.log(response);
        var data = response.data.objects;
        for (var i in data){
            allTaks.push(new Task(data[i]));
        }
    }, function () {
        console.log('ERROR');
    })

    theScope.todoList = allTaks;

    theScope.delete = function (taskId) {
        console.log('Deleting Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                theScope.todoList.splice(i, 1);
            }
        }
    };


    theScope.edit = function (taskId){
        console.log('Editing Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                Service.task = theScope.todoList[i].copy();
            }
        }
    };

    theScope.complete = function (taskId){
        console.log('Editing Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                theScope.todoList[i].complete();
            }
        }
    };

};

todoListController.$inject = ['$scope', 'Service', '$http', 'serviceUrl'];

todosProy.controller('todoListController', todoListController);



// Todo Controller

var todoController = function (theScope, Service) {

    theScope.Service = Service;

    theScope.task = {};

    theScope.$watch('Service.task', function () {
        theScope.task = theScope.Service.task;
    });

    theScope.create = function (form){
        allTaks.push(new Task(theScope.task));
        theScope.task = {};
    };

    theScope.edit = function (form){

        for (var i=0;i<allTaks.length;i++){
            if(allTaks[i].id == theScope.task.id){
                allTaks[i].title = theScope.task.title;
                allTaks[i].description = theScope.task.description;
            }
        }
        theScope.task = {};
    };

    theScope.isEditing = function () {
        if(theScope.task.id === undefined){
            return false;
        } else {
            return true;
        }
    };

    theScope.isCreating = function () {
        if(theScope.task.id === undefined){
            return true;
        } else {
            return false;
        }
    };

    theScope.cancel = function(){
        Service.task = {};
    };

};

todoController.$inject = ['$scope', 'Service'];

todosProy.controller('todoController', todoController);