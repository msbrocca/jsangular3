/**
 * Created by maxi on 4/2/16.
 */

var todosProy = angular.module('todosProy', []);

todosProy.factory('Service', function () {
    var Service = {
        task: {}

    };

    return Service;

});

todosProy.constant('serviceUrl', 'http://angularjs.agilehub.com.ar:8000/api/v1/');

todosProy.run(
    function ($http) {
        $http.defaults.headers.common.Authorization = 'Basic bXNicm9jY2E6cGFzc3dvcmQxMjM=';
    }
);