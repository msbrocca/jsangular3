/**
 * Created by maxi on 4/3/16.
 */

var lastTaskId = 0;

var Task = function (data) {

    this.id = data.id;
    this.title = data.name;
    this.description = data.description;
    this.done = data.status.id == 1 ? false : true;
};

Task.prototype.complete = function () {
    this.done = true;
}


var allTaks = [];
