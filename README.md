# README #

Codigo de los ejercicios de la unidad 3 del curso de Javascript / AngularJs

### Como hacer andar esto? ###

Se me hace que lo que hay que hacer es lo siguiente

1.- Descargar el codigo

2.- Parado en la parpeta principar ejecutar (como precondicion, tenes que tener instalado npm, gulp y bower - global):

    2.1.- $ npm install --save-dev gulp
    2.2.- $ npm install --save-dev bower
    2.3.- $ npm install gulp-concat gulp-sass gulp-connect gulp-rename gulp-jshint --save-dev
    2.4.- $ bower install bootstrap jquery angular --save-dev

3.- Correr $ gulp serve

4.- Desde el browser ir a http://localhost:9500/

5.- Mirar con compasion lo que se hizo