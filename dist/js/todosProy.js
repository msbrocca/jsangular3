/**
 * Created by maxi on 4/3/16.
 */

var lastTaskId = 0;

var Task = function (data) {

    var id = lastTaskId + 1;
    lastTaskId = id;
    this.id = id;
    this.title = data.title;
    this.description = data.description;
    this.done = false;
};

Task.prototype.complete = function () {
    this.done = true;
};

Task.prototype.copy = function () {
    return {
        id: this.id,
        title: this.title,
        description: this.description,
        done: this.done
    };
};

var allTaks = [];

var task1 = new Task({
    title: 'Task 1',
    description: 'Description Task 1'
});

var task2 = new Task({
    title: 'Task 2',
    description: 'Description Task 2'
});

var task3 = new Task({
    title: 'Task 3',
    description: 'Description Task 3'
});

var task4 = new Task({
    title: 'Task 4',
    description: 'Description Task 4'
});

allTaks.push(task1);
allTaks.push(task2);
allTaks.push(task3);
allTaks.push(task4);
/**
 * Created by maxi on 4/2/16.
 */

var todosProy = angular.module('todosProy', []);

todosProy.factory('Service', function () {
    var Service = {
        task: {}

    };

    return Service;

});

/**
 * Created by maxi on 4/2/16.
 */


// Todo List Controller
var todoListController = function (theScope, Service) {
    theScope.todoList = allTaks;

    theScope.delete = function (taskId) {
        console.log('Deleting Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                theScope.todoList.splice(i, 1);
            }
        }
    };


    theScope.edit = function (taskId){
        console.log('Editing Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                Service.task = theScope.todoList[i].copy();
            }
        }
    };

    theScope.complete = function (taskId){
        console.log('Editing Task: ' + taskId);
        for (var i=0;i<theScope.todoList.length;i++){
            if(theScope.todoList[i].id == taskId){
                theScope.todoList[i].complete();
            }
        }
    };

};

todoListController.$inject = ['$scope', 'Service'];

todosProy.controller('todoListController', todoListController);



// Todo Controller

var todoController = function (theScope, Service) {

    theScope.Service = Service;

    theScope.task = {};

    theScope.$watch('Service.task', function () {
        theScope.task = theScope.Service.task;
    });

    theScope.create = function (form){
        allTaks.push(new Task(theScope.task));
        theScope.task = {};
    };

    theScope.edit = function (form){

        for (var i=0;i<allTaks.length;i++){
            if(allTaks[i].id == theScope.task.id){
                allTaks[i].title = theScope.task.title;
                allTaks[i].description = theScope.task.description;
            }
        }
        theScope.task = {};
    };

    theScope.isEditing = function () {
        if(theScope.task.id === undefined){
            return false;
        } else {
            return true;
        }
    };

    theScope.isCreating = function () {
        if(theScope.task.id === undefined){
            return true;
        } else {
            return false;
        }
    };

    theScope.cancel = function(){
        Service.task = {};
    };

};

todoController.$inject = ['$scope', 'Service'];

todosProy.controller('todoController', todoController);
/**
 * Created by maxi on 4/3/16.
 */

function printList(){
    return {
        restrict: 'EA',
        templateUrl: 'app/todosProy/taskList.template.html',
        scope: true
    };
}

todosProy.directive('printList', printList);
/**
 * Created by maxi on 4/4/16.
 */

